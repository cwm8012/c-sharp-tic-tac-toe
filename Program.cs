﻿using System;

public class TicTacToe
{
    int boardLen = 9;
    public int x = 1;
    public int e = 0;
    public int o = -1;
    public int c = 2;
    int[] board = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    int turn = 1;

    public TicTacToe copyTicTacToe()
    {
        TicTacToe newTicTacToe = new TicTacToe();

        for (int i = 0; i < boardLen; i++)
        {
            newTicTacToe.board[i] = board[i];
        }
        newTicTacToe.turn = turn;

        return newTicTacToe;
    }

    public void printBoard()
    {
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine("-------------");
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                int index = i * 3 + j;
                if (board[index] == x)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("|");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(" X ");
                } else if (board[index] == o)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("|");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write(" O ");
                } else if (board[index] == e)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("|");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(" " + index + " ");
                }
            }
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("|");
            Console.WriteLine("-------------");
        }
        Console.WriteLine("");
    }

    public int gameState()
    {
        // check horizontal wins
        for (int i = 0; i < 3; i++)
        {
            int index = i *3;
            if (board[index] == board[index + 1] && board[index + 1] == board[index + 2] && board[index] != e)
            {
                return board[index];
            }
        }

        // check vertical wins
        for (int i = 0; i < 3; i++)
        {
            if (board[i] == board[i + 3] && board[i + 3] == board[i + 6] && board[i] != e)
            {
                return board[i];
            }
        }

        // check diagonal wins
        if (board[0] == board[4] && board[4] == board[8] && board[0] != e)
        {
            return board[0];
        }
        if (board[2] == board[4] && board[4] == board[6] && board[2] != e)
        {
            return board[2];
        }

        // check for full board
        for (int i = 0; i < boardLen; i++)
        {
            if (board[i] == e)
            {
                break;
            }

            if (i == boardLen - 1)
            {
                return e;
            }
        }

        return c;
    }

    private int miniMax(TicTacToe board, int turn)
    {
        int gameState = board.gameState();
        if (gameState != c)
        {
            return gameState;
        }

        TicTacToe playBoard = board.copyTicTacToe();

        if (turn == x)
        {
            int score = -10;
            for (int i = 0; i < boardLen; i++)
            {
                if (playBoard.board[i] == e)
                {
                    playBoard.board[i] = x;
                    score = Math.Max(score, miniMax(playBoard, o));
                    playBoard.board[i] = e;
                }
            }
            return score;
        } else {
            int score = 10;
            for (int i = 0; i < boardLen; i++)
            {
                if (playBoard.board[i] == e)
                {
                    playBoard.board[i] = o;
                    score = Math.Min(score, miniMax(playBoard, x));
                    playBoard.board[i] = e;
                }
            }
            return score;
        }
    }

    public void playMiniMaxMove()
    {
        if (gameState() != c)
        {
            return;
        }

        TicTacToe playBoard = this.copyTicTacToe();

        int bestScore = -10;
        int bestMove = -1;

        for (int i = 0; i < boardLen; i++)
        {
            if (playBoard.board[i] == e)
            {
                playBoard.board[i] = x;
                int score = miniMax(playBoard, o);
                playBoard.board[i] = e;

                if (score >= bestScore)
                {
                    bestScore = score;
                    bestMove = i;
                }
            }
        }

        if (bestMove != -1)
        {
            board[bestMove] = x;
        }
    }

    public void playHumanMove()
    {
        if (gameState() != c)
        {
            return;
        }

        int moveIndex = 10;

        while (moveIndex < 0 || moveIndex >= boardLen)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Select Move (0 - 8): ");
            moveIndex = Convert.ToInt32(Console.ReadLine());
            if (moveIndex < 0 || moveIndex >= boardLen)
            {
                if (board[moveIndex] != e)
                {
                    moveIndex = 10;
                }
            }
        }

        board[moveIndex] = o;
    }
}

class Program
{
    public static void Main(string[] args)
    {
        TicTacToe board = new TicTacToe();
        while (board.gameState() == board.c)
        {
            board.playMiniMaxMove();
            board.printBoard();
            board.playHumanMove();
        }

        int gameState = board.gameState();
        if (gameState == board.e)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Game ended in a draw.");
            board.printBoard();
        } else if (gameState == board.x)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("MiniMax won the game.");
            board.printBoard();
        } else if (gameState == board.o)
        {
            // this code will never run :)
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("You won the game.");
            board.printBoard();
        }
    }
}
